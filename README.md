Copyright &copy; 2020 Kirk Rader

# node-red-z2mqtt

Demonstrate Z-Wave and Zigbee device integration with [Node-RED](https://nodered.org/) using MQTT:

- Use MQTT messaging to drive device monitoring and control

- Use [zigbee2mqtt][] as a bridge between the Zigbee protocol and MQTT

- Implement a similar bridge between Z-Wave and MQTT using [node-red-contrib-openzwave][] and Node-RED's built-in support for MQTT

```mermaid
graph TD

  subgraph MQTT Server
    broker[MQTT Broker]
  end

  subgraph Node-RED Server

    subgraph Node-RED
      nodered[Node-RED Core]
      node-red-contrib-openzwave
    end

    openzwave[OpenZWave]

  end

  subgraph Zigbee Server
    zigbee2mqtt
  end

  zigbee-controller[Zigbee Adapter]
  zwave-controller[Z-Wave Controller]

  subgraph Zigbee Network
    zigbee-devices[Various Zigbee Devices]
  end

  subgraph Z-Wave Network
    zwave-devices[Various Z-Wave Devices]
  end

  broker --- | MQTT | zigbee2mqtt
  broker --- | MQTT | nodered
  zigbee2mqtt --- | USB | zigbee-controller
  zigbee-controller --- | Zigbee | zigbee-devices
  nodered --- node-red-contrib-openzwave
  node-red-contrib-openzwave --- openzwave
  openzwave --- | USB | zwave-controller
  zwave-controller --- | Z-Wave | zwave-devices
```

Note that _MQTT Server_, _Node-RED Server_ and _Zigbee Server_ are shown as logical groupings. They indicate which hardware and software components must be installed together on a single machine and which communicate via a network. Some or all of the components could be installed on a single machine. For example, I developed and debugged these flows in an environment where all three "servers" were really a single laptop. My production environment uses somewhat more elaborate versions of these flows running on a small number of Raspberry Pi's.

## Dependencies

### Hardware

- Zigbee adapter compatible with [zigbee2mqtt][]
    - I use [zzh! CC2652R Multiprotocol RF Stick](https://www.tindie.com/products/electrolama/zzh-cc2652r-multiprotocol-rf-stick/)
    - Any of the devices listed at <https://www.zigbee2mqtt.io/information/supported_adapters.html> should work
    - Be sure to follow the relevant instructions to use additional hardware and software that is required to flash your particular adapter with the appropriate firmware

- Z-Wave controller compatible with [OpenZWave][]
    - I use [Aeotec Z-Stick Gen5](https://aeotec.com/z-wave-usb-stick/)
    - Any Z-Wave controller that appears to the operating system as a serial device and is supported by [OpenZWave][] should work

- At least one each Z-Wave and Zigbee device to be paired with their respecctive controllers
    - These flows are designed to work as-is with a [Philips Hue](https://www.philips-hue.com/) "white and color ambiance" light and a [Zooz S2 Power Strip](https://www.getzooz.com/zooz-zen20-power-strip.html) as examples
    - You will need to modify them to match your makes and models of devices, Zigbee and Z-Wave network id's etc.

### Software

All of these packages must be installed using their respective instructions before importing these flows into Node-RED:

- MQTT broker
    - I host my own [Mosquitto](https://mosquitto.org) instance
    - A "cloud" based MQTT service will work with the corresponding performance, reliability, security and privacy issues
- [zigbee2mqtt][]
- [OpenZWave][]
- [node-red-dashboard][]
- [node-red-contrib-semaphore][]
- [node-red-contrib-openzwave][]
- [node-red-node-rbe][]

Notes:

- The Z-Wave controller device must be connected directly to the machine running Node-RED. The [OpenZWave][] library must be installed on that same machine before adding [node-red-contrib-openzwave][] to Node-RED's palette.

- The Zigbee adapter must be connected directly to the machine running [zigbee2mqtt][]. That machine can but does not need to be the same as that running Node-RED so long as both have network access to the same MQTT broker.

- The MQTT broker can but does not need to be running on the same machine as either Node-RED or [zigbee2mqtt][]. Again, all that is required is mutual network access.

## Usage

### Zigbee

Example dashboard for a full-color light (in particular, a Hue "white and color ambiance" light bulb) together with widgets to add, remove and configure devices in a Zigbee network:

![](./zigbee-dashboard.png)

The core functionality for the preceding dashboard is implemented by these flows:

![](./zigbee2mqtt-flow.png)

and

![](./luxo-flow.png)

The node labeled _luxo_ is an instance of this subflow, which maps the multiple attributes of a [zigbee2mqtt][] payload into separate messages for each of a number of corresponding dashboard controls:

![](./philips-hue-light-subflow.png)

Similarly, the node labeled _luxo HSB_ is an instance of the following subflow:

![](./philips-hue-hsb-subflow.png)

_HSB_ stands for _Hue, Saturation, Brightness_ and maps the corresponding device attributes to the parameters of a Node-RED color-picker dashboard control in HSV (Hue, Saturation, Value) mode.

Packaging the logic into subflows allows for easy re-use. If you have more than one Hue light paired with your Zigbee adapter you can easily add more instances of these subflows and address a particular device using the subflow's `${TOPIC}` parameter.

Note that all of these flows use what the [node-red-dashboard][] help text refers to as "closed loop feedback." This means that the controls change dynamically in response to changes in the state of the devices in real time. This can be seen by watching the positions of the _Hue_, _Saturation_ and _Brightness_ sliders change as you manipulate the color picker and vice versa. This requires the flow to send a _get_ message to [zigbee2mqtt][] to elicit the state of all devices so as to properly initialize the dashboard at startup.

### Z-Wave

Here is the example dashboard for monitoring and controlling the individual outlets, labeled _CH 1_ through _Ch 5_, in a Z-Wave enabled power strip:

![](zwave-dashboard.png)

Here is the flow that maps raw [OpenZWave][] messages to MQTT in a fashion similar to that provided by [zigbee2mqtt]{}:

![](zwave2mqtt-flow.png)

This flow uses such MQTT messages to implement the corresponding dashboard controls:

![](powerstrip-flow.png)

Once again, "closed loop feedback" and an initial set of "refresh value" commands are used to ensure that the state of the dashboard always reflects the current state of all devices.

#### Mapping OpenZWave to MQTT

_This section assumes familiarity with the concepts and terminology defined by [OpenZWave][] as wrapped by [node-red-contrib-openzwave][]. In particular, you should understand what is meant in this context by terms like "value id," "node id," "command class," "instance," "command index" etc._

The whole point of [zigbee2mqtt][] is to be a bridge between "raw" Zigbee messages and MQTT. See <https://www.zigbee2mqtt.io/information/mqtt_topics_and_message_structure.html> for information on the MQTT topics and payloads it defines.

One feature of these flows is to perform a similar function for Z-Wave. On the one hand, the [node-red-contrib-openzwave][] package translates "raw" [OpenZWave][] messages to and from Node-RED messages so this use of MQTT is not actually necessary to use Z-Wave devices in automation implemented in a single Node-RED instance to which a Z-Wave controller is directly attached. MQTT is extremely useful, however, for a variety of scenarios that go beyond such a simple, single-server architecture.

The _ad hoc_ topic and message structure defined for Z-Wave by these flows is simple and based directly on the JSON payloads defined by [node-red-contrib-openzwave][]. This makes these flows easy to integrate with off-the-shelf Node-RED components. It does mean that they inherit some oddities and asymmetries from [node-red-contrib-openzwave][].

The general pattern is that the payloads of status messages from [OpenZWave][] are passed through as-is as the payloads of corresponding MQTT messages. The topics for such messages are formed as follows[^zwave-nomenclature]:

- All Z-Wave related messages begin with `zwave/`

- If a status message contains an [OpenZWave][] "value id," the topic will be `zwave/<node id>/<command class>/<instance>/<command index>`

- If a status message pertains to a particular device but has no "value id" (e.g. a message announcing the activation of a new device on the Z-Wave network) it will have a topic like `zwave/<node id>/<sub-topic>` where `<sub-topic>` denotes a particular kind of event

- Otherwise, the topic will be of the form `zwave/<sub-topic>`

Sending command messages to Z-Wave devices uses a different pattern. All such messages are directed at particular "value ids" and so have the pattern `zwave/<sub-topic>/<node id>/<command class>/<instance>/<command index>`. In this case, `<sub-topic>` is either `get` or `set`.

For example, sending a MQTT message with the topic `zwave/get/6/37/1/0` sends an [OpenZWave][] _RefreshValue_ command to the "value id" _6-37-1-0_. The node whose id is 6 should respond asynchronously with a status message reporting the current on / off status of its switch with the instance id 1. That message will then be forwarded by these flows as a status message on the topic `zwave/6/37/1/0` when it arrives via [node-red-contrib-openzwave][]. The payloads of such `get` messages are ignored.

Conversely, sending a MQTT message to the topic `zwave/set/6/37/1/0` will issue an [OpenZWave][] "SetValue" command to control the on / off state of the switch that is instance 1 of node 6. The payload of such messages must be the value denoted by the given "value id." In the case of command class 37 (denoting a "basic switch" device state) this would be boolean `true` or `false` depending on whether the switch is to be turned on or off. This value matches that of the `currState` attribute in a corresponding `zwave/6/37/1/0` status message sent by the device. The asymmetry between the payloads of such command messages (which consists of just the "raw" value) and their corresponding status messages (which embeds the `currState` as one attribute among many describing the event which caused the message to be sent) is inherited from the underlying [node-red-contrib-openzwave][] Node-RED message and topic structures.

To summarize:

- This flow will forward incoming Z-Wave status messages as MQTT messages with topics that match the pattern `zwave/+/+/+/+`; the payloads will be exactly what was received from [node-red-contrib-openzwave][]

- To send a command to change the state of a device denoted by a given topic that matches `zwave/+/+/+/+`, send a MQTT message with the matching `zwave/set/+/+/+/+` topic; payload must be what would be returned as the value of the `currState` attribute in the payload from the given `zwave/+/+/+/+` message

- To elicit an asynchronous report of the current state of a given device attribute, send a MQTT message with the matching `zwave/get/+/+/+/+` topic; payload is ignored in this case

- Incoming messages from [node-red-contrib-openzwave][] are retained; i.e. MQTT messages with topics that match `zwave/+/+/+/+` will always have `retain` set to `true`

- Outgoing `get` and `set` messages should never be retained; i.e. messages with topics that match `zwave/get/+/+/+/+` and `zwave/set/+/+/+/+` should always be sent with `retain` set to `false`

[^zwave-nomenclature]: There is an unfortunate ambiguity between Z-Wave and Node-RED nomenclature. Both Node-RED and Z-Wave are documented as passing "messages" between "nodes" where each node has a unique "node id." It should be clear from context when "node" or "node id" is referring to a node in a Node-RED flow or a Z-Wave device node.

[openzwave]: http://www.openzwave.com/
[zigbee2mqtt]: https://www.zigbee2mqtt.io/
[node-red-contrib-openzwave]: https://flows.nodered.org/node/node-red-contrib-openzwave
[node-red-contrib-semaphore]: https://flows.nodered.org/node/node-red-contrib-semaphore
[node-red-node-rbe]: https://flows.nodered.org/node/node-red-node-rbe
[node-red-dashboard]: https://flows.nodered.org/node/node-red-dashboard
